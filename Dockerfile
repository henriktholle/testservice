FROM php:7.4-apache

RUN apt-get update
RUN apt-get -y install vim
RUN apt-get install -y git
RUN apt-get -y install zip

RUN echo "alias ll='ls -lha'" >> /root/.bashrc

# Apache
RUN a2enmod rewrite
RUN a2enmod ssl
RUN a2enmod headers

COPY ./docker/000-default.conf /etc/apache2/sites-enabled/
RUN mkdir -p /var/www/html/public/

# PHP
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/
RUN install-php-extensions mysqli
RUN install-php-extensions gettext
RUN install-php-extensions redis
RUN install-php-extensions mbstring
# RUN install-php-extensions solr
RUN install-php-extensions zip
RUN install-php-extensions bcmath
# RUN install-php-extensions amqp
RUN install-php-extensions mcrypt
RUN install-php-extensions pdo_mysql

COPY ./docker/.env /var/www/html
RUN sed -i 's/80/8080/' /etc/apache2/ports.conf
RUN sed -i 's/CustomLog/# CustomLog/' /etc/apache2/conf-available/other-vhosts-access-log.conf

RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
RUN sed -i 's/128M/512M/' /usr/local/etc/php/php.ini

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html
COPY . .
RUN mkdir -p ./storage/framework/sessions
RUN mkdir -p ./storage/framework/views
RUN mkdir -p ./storage/framework/cache/data
RUN chgrp -R www-data ./storage
RUN chown -R www-data ./storage/framework
RUN chmod -R g+w ./storage

RUN /usr/local/bin/composer update

RUN service apache2 restart

EXPOSE 8080
